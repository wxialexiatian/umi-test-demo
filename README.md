#  基于 Jest + Enzyme 的 UMI 单元测试

Install dependencies,

```bash
$ yarn
```

Start the dev server,

```bash
$ yarn start
```

##  Jest、Enzyme 介绍

Jest 是 Facebook 发布的一个开源的、基于 `Jasmine` 框架的 JavaScript 单元测试工具。提供了包括内置的测试环境 DOM API 支持、断言库、Mock 库等，还包含了 Spapshot Testing、 Instant Feedback 等特性。

Airbnb开源的 React 测试类库 Enzyme 提供了一套简洁强大的 API，并通过 jQuery 风格的方式进行DOM 处理，开发体验十分友好。不仅在开源社区有超高人气，同时也获得了React 官方的推荐。



## 说明

该demo功能比较简单，简单的增删查改。主要是针对于前端单元测试上手参照
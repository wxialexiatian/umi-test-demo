import React, { Component } from 'react';
import Header from '../../components/Header';
import UndoList from '../../components/UndoList';
import '../../common/style.less'

class TodoList extends Component  {
  state = {
    undoList: [], // 搜索框value值
  }
  
  // 获取Header里面 input框传过来的值
  addUndoItem = (value:any) =>{

    this.setState({
      undoList: [...this.state.undoList, value]
    })
  }

  // 通过传递过来的下标对数组进行删除
  deleteItem = (index:number) => {
    const newList = [...this.state.undoList];
    newList.splice(index, 1);
    this.setState({
      undoList:newList
    })
  }

  // 修改
  changeStatus = (index:number) => {
    console.log(index)
  }

  render() {
      const { undoList } = this.state; 

      return(
        <div className='app'>
          <Header addUndoItem={this.addUndoItem}></Header>
          <UndoList changeStatus = { this.changeStatus } deleteItem = { this.deleteItem } list = { undoList } ></UndoList>
        </div>
      )
  }
};
export default TodoList; 

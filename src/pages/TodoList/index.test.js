import React from 'react';
import Enzyme, { shallow  } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import TodoList from './index'

Enzyme.configure({ adapter: new Adapter() });

describe('TodoList组件相关', () => {
    it('TodoList 初始化列表为空', () => {
        const wrapper = shallow(<TodoList />);

        // 预期初始化时 state下的undoList 数组为空数组
        expect(wrapper.state('undoList')).toEqual([]);
    });

    it('TodoList 应该给 Header 传递一个增加 undoList 内容的方法', () => {
        const wrapper = shallow(<TodoList />);
        const Header = wrapper.find('Header');

        // 测试在TodoList组件上传递一个方法名为 addUndoItem 到组件Header里
        expect(Header.prop('addUndoItem')).toBe(wrapper.instance().addUndoItem);
    });

    it('当 Header 回车时，undoList 应该新增内容', () => {
        const wrapper = shallow(<TodoList />);
        const Header = wrapper.find('Header');
        // 从Header组件上找到一个addUndoItem方法
        const addFunc = Header.prop('addUndoItem');
        addFunc('w晚风'); // 调用这个方法进行传参

        // 上面调用了一次方法后 undoList 的长度应该增加到1
        expect(wrapper.state('undoList').length).toBe(1);
        // undoList数组里的第0位数据应该位w晚风
        expect(wrapper.state('undoList')[0]).toBe('w晚风');

        addFunc('w晚风2'); 
        // 这个时候期望长度位2
        expect(wrapper.state('undoList').length).toBe(2);
    });

    it('UndoList 应该接收 list, deleteItem, changeStatus 三个参数', () => {
        const wrapper = shallow(<TodoList />);
        const UndoList = wrapper.find('UndoList');
        
        // 期望向子组件传递了 list数据 与 deleteItem, changeStatus 方法
        expect(UndoList.prop('list')).toBeTruthy();
        expect(UndoList.prop('deleteItem')).toBeTruthy();
        expect(UndoList.prop('changeStatus')).toBeTruthy();
    });

    it('当deleteItem方法被执行时 undoList 应该删除内容', () => {
        const wrapper = shallow(<TodoList />);
        const undoList = ['李白','韩信','虞姬','小乔'];
        wrapper.setState({
            undoList
        })
        // TodoList 组件中调用  deleteItem 方法 
        wrapper.instance().deleteItem(0);

        expect(wrapper.state('undoList')).toEqual(['韩信','虞姬','小乔']);
    });
});
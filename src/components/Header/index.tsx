import React, { Component } from 'react';

interface IHeaderProps {
  addUndoItem: Function
}

class Header extends Component<IHeaderProps>  {
  state = {
      value:'', // 搜索框value值
  }

  // 监听input输入框改变 state 下 value值
  handleInputChange = (e:any) => {
    this.setState({
      value:e.target.value
    })
  }

  // 监听键盘回车事件 根据是否有值进行一系列操作
  handleInputKeyUp = (e:any) => {
    const { value } = this.state;

    if(e.keyCode === 13 && value){
      // 接收父组件传递过来的函数
      this.props.addUndoItem(value);

      // 设置完值后情况文本
      this.setState({
        value:''
      })
    }
  }

  render() {
      const { value } = this.state;

      return(
        <div className="header">
          <div className='header-content'>
            输入框:
            <input 
            placeholder='输入输入1'
            className='header-input'
            data-test='input' 
            value = { value } 
            onKeyUp = { this.handleInputKeyUp }
            onChange= { this.handleInputChange }></input>
          </div>
        </div>
      )
  }
};
 
export default Header; 

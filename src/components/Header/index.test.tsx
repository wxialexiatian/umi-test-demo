import React from 'react';
import Enzyme, { shallow  } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Header from './index'

Enzyme.configure({ adapter: new Adapter() });

describe('Header组件相关', () => {
    it('Header 渲染样式正常', () => {
        const wrapper = shallow(<Header />);
        
        // expect(wrapper).toMatchSnapshot();// 进行快照保存
        expect(wrapper.debug()).toMatchSnapshot();
    });

    it('Header 组件包含一个 input 框', () => {
        const wrapper = shallow(<Header />);
        // 根据属性进行查找
        const inputElem = wrapper.find("[data-test='input']")
        console.log(wrapper.debug); // 进行测试调试
        expect(inputElem.length).toBe(1);
    });

    it('Header 组件 input 框组件，初始化应该为空', () => {
        const wrapper = shallow(<Header />);
        const inputElem = wrapper.find("[data-test='input']")
        expect(inputElem.prop('value')).toEqual('');
    });

    it('Header 组件 input 框内容，当用户输入时，会跟随变化', () => {
        const wrapper = shallow(<Header />);
        const inputElem = wrapper.find("[data-test='input']");
        const userInput = 'w晚风';
        
        // simulate 模拟 触发input框的 change事件
        inputElem.simulate('change',{
            target:{
                value:userInput
            }
        })

        // 调用Header中的state，对比state下的value值是否跟上面模拟相匹配
        expect(wrapper.state('value')).toEqual(userInput); // 这里是对用户操作后组件里的数据做测试

        // 第二种方法 我这里的逻辑是根据上面一起测试的 所以要是放开这段代码 请不要把上面的代码注释 这只是测试的两种方式 一种针对数据 一种针对dom
        // const inputElem2 = wrapper.find("[data-test='input']");
        // expect(inputElem2.prop('value')).toBe(userInput); // 这里是用户操作后对dom的属性做测试，
    });

    it('Header 组件 input 框输入回车时，如果 input 无内容，无操作', () => {
        const fn = jest.fn(); // 这是jest的一个模客方法

        const wrapper = shallow(<Header addUndoItem = { fn } />);
        const inputElem = wrapper.find("[data-test='input']");
        // 先对组件state里的value重置为空
        wrapper.setState({value:''})
        inputElem.simulate('keyUp',{
            keyCode:13
        });
        // 在没用内容的情况下不调用函数
        expect(fn).not.toHaveBeenCalled()
    });

    it('Header 组件 input 框输入回车时，如果 input 有内容，函数应该被调用', () => {
        const fn = jest.fn(); // 这是jest的一个模客方法

        const wrapper = shallow(<Header addUndoItem = { fn } />);
        const inputElem = wrapper.find("[data-test='input']");
        
        wrapper.setState({ value:'w晚风' });
        // 模拟键盘回车事件 
        inputElem.simulate('keyUp',{
            keyCode:13
        });

        // 在有内容的情况下 函数 应该被调用
        expect(fn).toHaveBeenCalled();

        // 在input框有内容的情况下，最后执行完方法后 应该清楚掉文本内容
        const inputElem2 = wrapper.find("[data-test='input']");
        expect(inputElem2.prop('value')).toBe('');

        // 也可以去掉上面两行直接这样去组件里面拿
        // expect(wrapper.state('value')).toBe('');
    });
});